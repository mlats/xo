package com.example.xoapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private var FirstPlayer: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        resetbutton.setOnClickListener {
            button00.text=""
            button00.isClickable = true
            button01.text=""
            button01.isClickable = true
            button02.text=""
            button02.isClickable = true
            button10.text=""
            button10.isClickable = true
            button11.text=""
            button11.isClickable = true
            button12.text=""
            button12.isClickable = true
            button20.text=""
            button20.isClickable = true
            button21.text=""
            button21.isClickable = true
            button22.text=""
            button22.isClickable = true
            FirstPlayer=true

        }
        button00.setOnClickListener {
            StatusChecker(button00)
            CheckWinner()
        }
        button01.setOnClickListener {
            StatusChecker(button01)
            CheckWinner()
        }
        button02.setOnClickListener {
            StatusChecker(button02)
            CheckWinner()
        }
        button10.setOnClickListener {
            StatusChecker(button10)
            CheckWinner()
        }
        button11.setOnClickListener {
            StatusChecker(button11)
            CheckWinner()
        }
        button12.setOnClickListener {
            StatusChecker(button12)
            CheckWinner()
        }
        button20.setOnClickListener {
            StatusChecker(button20)
            CheckWinner()
        }
        button21.setOnClickListener {
            StatusChecker(button21)
            CheckWinner()
        }
        button22.setOnClickListener {
            StatusChecker(button22)
            CheckWinner()
        }

    }

    private fun StatusChecker(button: Button) {
        if (FirstPlayer) {
            button.text = "x"
        } else {
            button.text = "O"
        }
        button.isClickable = false
        FirstPlayer=false
    }
    private fun CheckWinner(){
        if(button00.text.toString().isNotEmpty() && button00.text.toString() == button01.text.toString() && button00.text.toString() == button02.text.toString())
        { Toast.makeText(this, "Winner is ${button00.text.toString()}", Toast.LENGTH_SHORT).show()
           UnableClick()}
        else if (button00.text.toString().isNotEmpty() && button00.text.toString() == button10.text.toString() && button00.text.toString() == button20.text.toString())
        { Toast.makeText(this, "Winner is ${button00.text.toString()}", Toast.LENGTH_SHORT).show()
            UnableClick()}
        else if (button00.text.toString().isNotEmpty() && button00.text.toString() == button12.text.toString() && button00.text.toString() == button22.text.toString())
        { Toast.makeText(this, "Winner is ${button00.text.toString()}", Toast.LENGTH_SHORT).show()
            UnableClick()}
        else if(button10.text.toString().isNotEmpty() && button10.text.toString() == button11.text.toString() && button10.text.toString() == button12.text.toString())
            { Toast.makeText(this, "Winner is ${button10.text.toString()}", Toast.LENGTH_SHORT).show()
                UnableClick()}
        else if(button20.text.toString().isNotEmpty() && button20.text.toString() == button21.text.toString() && button20.text.toString() == button22.text.toString())
        { Toast.makeText(this, "Winner is ${button20.text.toString()}", Toast.LENGTH_SHORT).show()
            UnableClick()}
        else if (button20.text.toString().isNotEmpty() && button20.text.toString() == button11.text.toString() && button20.text.toString() == button02.text.toString())
        { Toast.makeText(this, "Winner is ${button20.text.toString()}", Toast.LENGTH_SHORT).show()
            UnableClick()}
        else if(button00.text.toString().isNotEmpty() && button01.text.toString().isNotEmpty() && button02.text.toString().isNotEmpty() && button10.text.toString().isNotEmpty() && button11.text.toString().isNotEmpty() &&
                button12.text.toString().isNotEmpty() && button20.text.toString().isNotEmpty() && button21.text.toString().isNotEmpty() && button22.text.toString().isNotEmpty()){
            Toast.makeText(this, "It's a tie!", Toast.LENGTH_SHORT).show() }
        }

        private fun UnableClick(){
            button00.isClickable=false
            button01.isClickable=false
            button02.isClickable=false
            button10.isClickable=false
            button11.isClickable=false
            button12.isClickable=false
            button20.isClickable=false
            button21.isClickable=false
            button22.isClickable=false
        }
    }

